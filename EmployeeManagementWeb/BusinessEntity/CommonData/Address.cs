﻿
/**********************************************************************************************************************************/
/* File Name: Address.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.Entity
{
    public class Address
    {
        /// <summary>
        /// Employee address - building name
        /// </summary>
        public string BuildingName { get; set; }

        /// <summary>
        /// Postal code of employee
        /// </summary>
        public int? PostalCode { get; set; }

        /// <summary>
        /// Employee City 
        /// </summary>
        public City City { get; set; }

        /// <summary>
        /// Employee State
        /// </summary>
        public State State { get; set; }
    }
}
