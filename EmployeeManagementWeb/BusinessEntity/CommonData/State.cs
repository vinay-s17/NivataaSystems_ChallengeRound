﻿using System;

/**********************************************************************************************************************************/
/* File Name: State.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.Entity
{
    public class State
    {
        /// <summary>
        /// Primary Id of City
        /// </summary>
        public Guid StateId { get; set; }

        /// <summary>
        /// City Name as string
        /// </summary>
        public string StateName { get; set; }
    }
}
