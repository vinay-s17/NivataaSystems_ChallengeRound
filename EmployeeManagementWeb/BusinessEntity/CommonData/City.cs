﻿using System;

/**********************************************************************************************************************************/
/* File Name: City.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/


namespace EmployeeManagement.Entity
{
    public class City
    {
        /// <summary>
        /// Primary Id of City
        /// </summary>
        public Guid CityId { get; set; }

        /// <summary>
        /// City Name as string
        /// </summary>
        public string CityName { get; set; }
    }
}
