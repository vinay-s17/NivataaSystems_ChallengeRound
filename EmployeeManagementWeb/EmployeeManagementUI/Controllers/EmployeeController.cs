﻿using EmployeeManagement.BusinessComponents;
using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using EmployeeManagement.Model;
using System.Web.Mvc;
using AutoMapper;
using Elmah;

namespace EmployeeManagement.Controllers
{
    /// <summary>
    /// Employee controller - Deals with all employee related operations
    /// </summary>
    public class EmployeeController : Controller
    {
        #region Constants
        private const string ACTION_ERROR_PAGE_URL = "~/Views/Shared/Error.cshtml";
        private const string ACTION_INDEX = "Index";
        #endregion

        #region Properties

        private IReferenceDataOperations refDataOperations;
        private IEmployeeOperations employeeOperations;

        #endregion

        #region Constructors

        public EmployeeController(IReferenceDataOperations refDataOperations, IEmployeeOperations employeeOperations)
        {
            this.refDataOperations = refDataOperations;
            this.employeeOperations = employeeOperations;
        }

        #endregion

        #region Helper Methods - Get Methods for Common pages

        #region Get All States

        /// <summary>
        /// To Get all States as List
        /// </summary>
        /// <returns></returns>
        [OutputCache(Duration = 3600, VaryByParam = "none")]
        public JsonResult GetAllStatesList()
        {
            List<State> statesList = null;
            try
            {
                //Dependent on Dataoperations to load AllStates data from DB
                statesList = refDataOperations.GetAllStates();
                return Json(statesList.Select(item => new SelectListItem { Text = item.StateName, Value = item.StateId.ToString() }).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(statesList);
            }

        }
        #endregion
        
        #region Get All Cities based on State ID

        /// <summary>
        /// To Get all Cities as List based on State ID
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllCitiesList(Guid? stateId)
        {
            List<City> citiesList = null;
            try
            {
                if (stateId != null)
                    citiesList = refDataOperations.GetAllCities(stateId.GetValueOrDefault());
                else
                    citiesList = new List<City>();
                return Json(citiesList.Select(item => new SelectListItem { Text = item.CityName, Value = item.CityId.ToString() }).ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Error handler - Using Elmah
                ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(citiesList);
            }

        }

        #endregion

        #endregion

        #region EmployeeDirectory - Home / Index Action Result
        /// <summary>
        /// Landing page Home / Index Action Result
        /// </summary>
        /// <param name="ipModel"></param>
        /// <returns></returns>
        [OutputCache(Duration = 0, VaryByParam = "*")]
        public ActionResult Index(EmployeeFilterViewModel ipModel)
        {
            try
            {
                //Load all Employees as List for populating the Grid
                var DataSource = employeeOperations.GetAllEmployees(ipModel.FilterText);
                ViewBag.DataSource = DataSource;
                return View(ipModel);
            }
            catch(Exception ex)
            {
                //Error handler - Using Elmah
                ErrorSignal.FromCurrentContext().Raise(ex);
                return View(ACTION_ERROR_PAGE_URL);
            }
            
        }
        #endregion

        #region Delete Employee - Action Result
        /// <summary>
        /// Delete Employee by Id
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public void DeleteEmployee(Guid employeeId)
        {
            try
            {
                employeeOperations.DeleteEmployee(employeeId);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                //return View(ACTION_ERROR_PAGE_URL);
            }
        }
        #endregion

        #region Create or Edit Employee - Action Result

        #region Create or Edit - Get Action
        /// <summary>
        /// Create or Edit employee - Get Action
        /// If employeeUid = null -> Treated as new Employee - hence creation
        /// If employeeUid != null -> Treated as Update employee details
        /// </summary>
        /// <param name="employeeUid"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateOrEditEmployeeProfile(Guid? employeeUid)
        {
            EmployeeViewModel newEmployeeobj = null;
            if (employeeUid == null)
            {
                newEmployeeobj = new EmployeeViewModel();
                newEmployeeobj.HomeAddress = new AddressViewModel();
                newEmployeeobj.HomeAddress.State = new StateViewModel();
                newEmployeeobj.HomeAddress.City = new CityViewModel();
                newEmployeeobj.PostalAddress = new AddressViewModel();
                newEmployeeobj.PostalAddress.State = new StateViewModel();
                newEmployeeobj.PostalAddress.City = new CityViewModel();
                newEmployeeobj.IsPostalAddrSameAsHomeAddr = true;
            }
            else
            {
                Employee employeeDetails = employeeOperations.GetEmployeeById(employeeUid.GetValueOrDefault());
                newEmployeeobj = Mapper.DynamicMap<Employee, EmployeeViewModel>(employeeDetails);
            }

            return View(newEmployeeobj);
        }
        #endregion

        #region Create or Edit - Post Action
        /// <summary>
        /// Create or Edit - Post action
        /// Writes DAO in database
        /// </summary>
        /// <param name="employeeObj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateOrEditEmployeeProfile(EmployeeViewModel employeeObj)
        {
            try
            {
                if (employeeObj.IsPostalAddrSameAsHomeAddr)
                    employeeObj.PostalAddress = employeeObj.HomeAddress;

                Employee employeeDao = Mapper.DynamicMap<EmployeeViewModel, Employee>(employeeObj);

                employeeOperations.CreateOrUpdateEmployee(employeeDao);

                return RedirectToAction(ACTION_INDEX);
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
                return View(ACTION_ERROR_PAGE_URL);
            }
        }
        #endregion
        #endregion
    }
}