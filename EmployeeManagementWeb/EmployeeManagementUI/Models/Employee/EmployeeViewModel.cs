﻿using System;
/**********************************************************************************************************************************/
/* File Name: EmployeeViewModel.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.Model
{
    public class EmployeeViewModel
    {
        /// <summary>
        /// Primary Id (Guid id ie for Salt) of Employee
        /// </summary>
        public Guid EmployeeUid { get; set; }

        /// <summary>
        /// Unique Id / Display id of Employee
        /// </summary>
        public int EmployeeDisplayId { get; set; }

        /// <summary>
        /// Employee first name
        /// </summary>
        public string EmployeeFirstName { get; set; }

        /// <summary>
        /// Employee last name
        /// </summary>
        public string EmployeeLastName { get; set; }

        /// <summary>
        /// Employee Home address
        /// </summary>
        public AddressViewModel HomeAddress { get; set; }

        /// <summary>
        /// Employee Postal address
        /// </summary>
        public AddressViewModel PostalAddress { get; set; }

        /// <summary>
        /// Postal address is same as Home address
        /// </summary>
        public bool IsPostalAddrSameAsHomeAddr { get; set; }
    }
}