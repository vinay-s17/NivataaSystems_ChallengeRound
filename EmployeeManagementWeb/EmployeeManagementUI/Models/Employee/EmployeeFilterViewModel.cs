﻿using System;
/**********************************************************************************************************************************/
/* File Name: EmployeeFilterViewModel.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.Model
{
    public class EmployeeFilterViewModel
    {
        /// <summary>
        /// Employee search using String field.
        /// </summary>
        public string FilterText { get; set; }
    }
}