﻿using System;

/**********************************************************************************************************************************/
/* File Name: StateViewModel.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace EmployeeManagement.Model
{
    public class StateViewModel
    {
        /// <summary>
        /// Primary Id of City
        /// </summary>
        public Guid StateId { get; set; }

        /// <summary>
        /// City Name as string
        /// </summary>
        public string StateName { get; set; }
    }
}