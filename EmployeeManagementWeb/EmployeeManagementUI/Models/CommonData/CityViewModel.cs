﻿using System;
/**********************************************************************************************************************************/
/* File Name: CityViewModel.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace EmployeeManagement.Model
{
    public class CityViewModel
    {
        /// <summary>
        /// Primary Id of City
        /// </summary>
        public Guid CityId { get; set; }

        /// <summary>
        /// City Name as string
        /// </summary>
        public string CityName { get; set; }
    }
}