﻿using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;


/**********************************************************************************************************************************/
/* File Name: IReferenceDataRepository.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    /// <summary>
    /// Interface for Reference Data Module Repository
    /// </summary>
    public interface IReferenceDataRepository
    {
        #region Get All Cities as List

        /// <summary>
        /// Get All Cities as List
        /// </summary>
        /// <returns></returns>

        List<City> GetAllCities(Guid stateId);

        #endregion
       
        #region Get All States as List

        /// <summary>
        /// Get All States
        /// </summary>
        /// <returns></returns>

        List<State> GetAllStates();

        #endregion
        
    }
}
