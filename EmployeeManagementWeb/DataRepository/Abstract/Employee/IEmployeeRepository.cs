﻿using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;


/**********************************************************************************************************************************/
/* File Name: IEmployeeRepository.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    /// <summary>
    /// Interface for Reference Data Module Repository
    /// </summary>
    public interface IEmployeeRepository
    {
        #region Get All Employees as List

        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <returns></returns>
        List<Employee> GetAllEmployees(string filterText);

        #endregion

        #region Get Employe by UId

        /// <summary>
        /// Get Employe by Id
        /// </summary>
        /// <returns></returns>
        Employee GetEmployeeById(Guid employeeUid);

        #endregion

        #region Create or Update Employee

        /// <summary>
        /// Create or Update Employee
        /// </summary>
        /// <returns></returns>
        void CreateOrUpdateEmployee(Employee employeeDao);

        #endregion


        #region Delete employee

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        void DeleteEmployee(Guid employeeUid);

        #endregion
    }
}
