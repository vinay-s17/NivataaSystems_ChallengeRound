﻿using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;
/**********************************************************************************************************************************/
/* File Name: EmployeeProcedureList.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    class EmployeeProcedureList
    {
        #region Get All Employees as List

        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <returns></returns>
        public static List<Employee> GetAllEmployees(string filterText)
        {
            try
            {
                return GetAllEmployeesSP.Execute(filterText);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get Employe by UId

        /// <summary>
        /// Get Employe by Id
        /// </summary>
        /// <returns></returns>
        public static Employee GetEmployeeById(Guid employeeUid)
        {
            try
            {
                return GetEmployeeByIdSP.Execute(employeeUid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        #region Create or Update Employee

        /// <summary>
        /// Create or Update Employee
        /// </summary>
        /// <returns></returns>
        public static void CreateOrUpdateEmployee(Employee employeeDao)
        {
            try
            {
                CreateOrUpdateEmployeeSP.Execute(employeeDao);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Delete employee

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        public static void DeleteEmployee(Guid employeeUid)
        {
            try
            {
                DeleteEmployeeSP.Execute(employeeUid);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
