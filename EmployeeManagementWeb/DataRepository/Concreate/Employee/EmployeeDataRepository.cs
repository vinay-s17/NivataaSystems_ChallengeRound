﻿using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;
/**********************************************************************************************************************************/
/* File Name: EmployeeDataRepository.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: Database Reference Data class to handle all the database related operations with respect to EmployeeDataRepository Module
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    public class EmployeeDataRepository : IEmployeeRepository
    {
        #region Get All Employees as List

        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <returns></returns>
        public List<Employee> GetAllEmployees(string filterText)
        {
            return EmployeeProcedureList.GetAllEmployees(filterText);
        }

        #endregion

        #region Get Employe by UId

        /// <summary>
        /// Get Employe by Id
        /// </summary>
        /// <returns></returns>
        public Employee GetEmployeeById(Guid employeeUid)
        {
            return EmployeeProcedureList.GetEmployeeById(employeeUid);
        }

        #endregion

        #region Create or Update Employee

        /// <summary>
        /// Create or Update Employee
        /// </summary>
        /// <returns></returns>
        public void CreateOrUpdateEmployee(Employee employeeDao)
        {
            EmployeeProcedureList.CreateOrUpdateEmployee(employeeDao);
        }

        #endregion

        #region Delete employee

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        public void DeleteEmployee(Guid employeeUid)
        {
            EmployeeProcedureList.DeleteEmployee(employeeUid);
        }

        #endregion
    }
}
