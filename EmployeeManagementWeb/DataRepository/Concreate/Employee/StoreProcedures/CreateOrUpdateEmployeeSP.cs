﻿using System;
using NS.EntityRepositories.Database;
using EmployeeManagement.Entity;
using NS.Repositories.DataAccess;

/**********************************************************************************************************************************/
/* File Name: CreateOrUpdateEmployeeSP.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: Used for Create or Update Employee details
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace DataRepository
{
    class CreateOrUpdateEmployeeSP : StoreProc
    {
        #region Parameters and Constant

        public Employee employeeDao = null;

        #endregion

        #region Constructors
        /// <summary>
        /// CreateOrUpdateEmployeeSP Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public CreateOrUpdateEmployeeSP(Employee employeeDao, string ConnectionString) : base(ConnectionString)
        {
            this.StoreProcName = "NS_CREATEORUPDATE_EMPLOYEE_DETAILS_PROC";
            this.employeeDao = employeeDao;
        }
        #endregion

        #region Map Input
        /// <summary>
        /// Map Input
        /// </summary>
        /// <returns></returns>
        public override bool MapInput()
        {
            AddInParameter("EMP_FNAME", ParamType.String, employeeDao.EmployeeFirstName);
            AddInParameter("EMP_LNAME", ParamType.String, employeeDao.EmployeeLastName); 

            AddInParameter("STATE_ID_HOME", ParamType.Guid, employeeDao.HomeAddress.State.StateId);
            AddInParameter("CITY_ID_HOME", ParamType.Guid, employeeDao.HomeAddress.City.CityId);
            AddInParameter("BUILDING_NAME_HOME", ParamType.String, employeeDao.HomeAddress.BuildingName);
            AddInParameter("POSTAL_CODE_HOME", ParamType.Int32, employeeDao.HomeAddress.PostalCode.GetValueOrDefault());

            AddInParameter("STATE_ID_POSTAL", ParamType.Guid, employeeDao.PostalAddress.State.StateId);
            AddInParameter("CITY_ID_POSTAL", ParamType.Guid, employeeDao.PostalAddress.City.CityId);
            AddInParameter("BUILDING_NAME_POSTAL", ParamType.String, employeeDao.PostalAddress.BuildingName);
            AddInParameter("POSTAL_CODE_POSTAL", ParamType.Int32, employeeDao.PostalAddress.PostalCode.GetValueOrDefault());

            if(employeeDao.EmployeeUid != null && employeeDao.EmployeeUid != Guid.Empty)
                AddInParameter("EMP_UID", ParamType.Guid, employeeDao.EmployeeUid);
            return true;
        }
        #endregion

        #region Map Output

        /// <summary>
        /// Map Output
        /// </summary>
        /// <param name="reader"></param>
        public override void MapOutput(System.Data.IDataReader reader)
        {  
        }

        #endregion

        #region ExcecuteSP
        /// <summary>
        /// Method for Excecution of SP
        /// </summary>
        /// <returns></returns>
        public static void Execute(Employee employeeDao)
        {
            CreateOrUpdateEmployeeSP storedProcedure = new CreateOrUpdateEmployeeSP(employeeDao, ConnectionStringProvider.ConnectionString);
            storedProcedure.ExecuteSP();
        }
        #endregion
    }
}
