﻿using System;
using NS.EntityRepositories.Database;
using NS.Repositories.DataAccess;

/**********************************************************************************************************************************/
/* File Name: DeleteEmployeeSP.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: Used for Deleting an Employee from DB
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace DataRepository
{
    class DeleteEmployeeSP : StoreProc
    {
        #region Parameters and Constant

        public Guid employeeUid { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// DeleteEmployeeSP Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public DeleteEmployeeSP(Guid employeeUid, string ConnectionString) : base(ConnectionString)
        {
            this.StoreProcName = "NS_DELETE_EMPLOYEE_PROC";
            this.employeeUid = employeeUid;
        }
        #endregion

        #region Map Input
        /// <summary>
        /// Map Input
        /// </summary>
        /// <returns></returns>
        public override bool MapInput()
        {
            AddInParameter("EMP_UID", ParamType.Guid, employeeUid);
            return true;
        }
        #endregion

        #region Map Output

        /// <summary>
        /// Map Output
        /// </summary>
        /// <param name="reader"></param>
        public override void MapOutput(System.Data.IDataReader reader)
        {
        }

        #endregion

        #region ExcecuteSP
        /// <summary>
        /// Method for Excecution of SP
        /// </summary>
        /// <returns></returns>
        public static void Execute(Guid employeeUid)
        {
            DeleteEmployeeSP storedProcedure = new DeleteEmployeeSP(employeeUid, ConnectionStringProvider.ConnectionString);
            storedProcedure.ExecuteSP();
        }
        #endregion
    }
}
