﻿using System;
using NS.EntityRepositories.Database;
using System.Collections.Generic;
using EmployeeManagement.Entity;
using NS.Repositories.DataAccess;

/**********************************************************************************************************************************/
/* File Name: GetAllEmployeesSP.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: Used for getting all employee details as List
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace DataRepository
{
    class GetAllEmployeesSP : StoreProc
    {
        #region Parameters and Constant

        public List<Employee> lstEmployees = null;
        public string filterText { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// GetAllCitiesSP Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public GetAllEmployeesSP(string filterText, string ConnectionString) : base(ConnectionString)
        {
            this.StoreProcName = "NS_GETALL_EMPLOYEES_PROC";
            this.filterText = filterText;
        }
        #endregion

        #region Map Input
        /// <summary>
        /// Map Input
        /// </summary>
        /// <returns></returns>
        public override bool MapInput()
        {
            if(filterText != null && filterText != string.Empty)
                AddInParameter("FILTER_TEXT", ParamType.String, filterText);
            return true;
        }
        #endregion

        #region Map Output

        /// <summary>
        /// Map Output
        /// </summary>
        /// <param name="reader"></param>
        public override void MapOutput(System.Data.IDataReader reader)
        {
            string address = string.Empty;
            string[] localeAddress = null;
            if (reader != null)
            {
                lstEmployees = new List<Employee>();
               
                while (reader.Read())
                {
                    Employee empDao = new Employee();
                    empDao.EmployeeFirstName = HandleOutputParamValue<string>.Get(reader, "EmpFName");
                    empDao.EmployeeLastName = HandleOutputParamValue<string>.Get(reader, "EmpLName");
                    empDao.HomeAddress = new Address();
                    empDao.HomeAddress.State = new State();
                    empDao.HomeAddress.City = new City();
                    address = HandleOutputParamValue<string>.Get(reader, "ADDRESS");
                    localeAddress = address.Split('^');
                    empDao.HomeAddress.State.StateName =(localeAddress[0].Split('|')[0]);
                    empDao.HomeAddress.City.CityName = (localeAddress[0].Split('|')[1]);
                    empDao.HomeAddress.BuildingName = localeAddress[0].Split('|')[2];
                    empDao.HomeAddress.PostalCode = Int32.Parse(localeAddress[0].Split('|')[3]);
                    empDao.PostalAddress = new Address();
                    empDao.PostalAddress.State = new State();
                    empDao.PostalAddress.City = new City();
                    empDao.PostalAddress.State.StateName = (localeAddress[1].Split('|')[0]);
                    empDao.PostalAddress.City.CityName = (localeAddress[0].Split('|')[1]);
                    empDao.PostalAddress.BuildingName = localeAddress[1].Split('|')[2];
                    empDao.PostalAddress.PostalCode = Int32.Parse(localeAddress[1].Split('|')[3]);                    
                    empDao.EmployeeUid = HandleOutputParamValue<Guid>.Get(reader, "EmpUid");
                    empDao.EmployeeDisplayId = HandleOutputParamValue<int>.Get(reader, "EmpDisplayid");
                    lstEmployees.Add(empDao);
                    empDao = null;
                }
            }
        }

        #endregion

        #region ExcecuteSP
        /// <summary>
        /// Method for Excecution of SP
        /// </summary>
        /// <returns></returns>
        public static List<Employee> Execute(string filterText)
        {
            GetAllEmployeesSP storedProcedure = new GetAllEmployeesSP(filterText, ConnectionStringProvider.ConnectionString);
            storedProcedure.ExecuteSP();
            return storedProcedure.lstEmployees;
        }
        #endregion


    }
}
