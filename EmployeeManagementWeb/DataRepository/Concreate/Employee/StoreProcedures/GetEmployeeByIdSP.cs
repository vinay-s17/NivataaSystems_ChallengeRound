﻿using System;
using NS.EntityRepositories.Database;
using EmployeeManagement.Entity;
using NS.Repositories.DataAccess;

/**********************************************************************************************************************************/
/* File Name: GetEmployeeByIdSP.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: used for Getting Employee detail by passing EmployeeUID
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace DataRepository
{
    class GetEmployeeByIdSP : StoreProc
    {
        #region Parameters and Constant

        public Employee empDao = null;
        public Guid employeeUid { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// GetEmployeeByIdSP Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public GetEmployeeByIdSP(Guid employeeUid, string ConnectionString) : base(ConnectionString)
        {
            this.StoreProcName = "NS_GET_EMPLOYE_BY_ID_PROC";
            this.employeeUid = employeeUid;
        }
        #endregion

        #region Map Input
        /// <summary>
        /// Map Input
        /// </summary>
        /// <returns></returns>
        public override bool MapInput()
        {
            AddInParameter("EMP_UID", ParamType.Guid, employeeUid);
            return true;
        }
        #endregion

        #region Map Output

        /// <summary>
        /// Map Output
        /// </summary>
        /// <param name="reader"></param>
        public override void MapOutput(System.Data.IDataReader reader)
        {
            string address = string.Empty;
            string[] localeAddress = null;

            if (reader != null)
            {
               while (reader.Read())
                {
                    empDao = new Employee();
                    empDao.EmployeeFirstName = HandleOutputParamValue<string>.Get(reader, "EmpFName");
                    empDao.EmployeeLastName = HandleOutputParamValue<string>.Get(reader, "EmpLName");
                    empDao.HomeAddress = new Address();
                    empDao.HomeAddress.State = new State();
                    empDao.HomeAddress.City = new City();
                    address = HandleOutputParamValue<string>.Get(reader, "ADDRESS");
                    localeAddress = address.Split('^');
                    empDao.HomeAddress.State.StateId = new Guid(localeAddress[0].Split('|')[0]);
                    empDao.HomeAddress.City.CityId = new Guid(localeAddress[0].Split('|')[1]);
                    empDao.HomeAddress.BuildingName = localeAddress[0].Split('|')[2];
                    empDao.HomeAddress.PostalCode = Int32.Parse(localeAddress[0].Split('|')[3]);
                    empDao.PostalAddress = new Address();
                    empDao.PostalAddress.State = new State();
                    empDao.PostalAddress.City = new City();
                    empDao.PostalAddress.State.StateId = new Guid(localeAddress[1].Split('|')[0]);
                    empDao.PostalAddress.City.CityId = new Guid(localeAddress[1].Split('|')[1]);
                    empDao.PostalAddress.BuildingName = localeAddress[1].Split('|')[2];
                    empDao.PostalAddress.PostalCode = Int32.Parse(localeAddress[1].Split('|')[3]);
                    empDao.EmployeeUid = HandleOutputParamValue<Guid>.Get(reader, "EmpUid");
                    empDao.EmployeeDisplayId = HandleOutputParamValue<int>.Get(reader, "EmpDisplayid");
                    empDao.EmployeeUid = employeeUid;
                }
            }
        }

        #endregion

        #region ExcecuteSP
        /// <summary>
        /// Method for Excecution of SP
        /// </summary>
        /// <returns></returns>
        public static Employee Execute(Guid employeeUid)
        {
            GetEmployeeByIdSP storedProcedure = new GetEmployeeByIdSP(employeeUid, ConnectionStringProvider.ConnectionString);
            storedProcedure.ExecuteSP();
            return storedProcedure.empDao;
        }
        #endregion

    }
}
