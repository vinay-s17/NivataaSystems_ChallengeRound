﻿/**********************************************************************************************************************************/
/* File Name: StoreProc.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
using System.Web.Configuration;

namespace NS.EntityRepositories.Database
{
    /// <summary>
    /// Connection string provider class
    /// </summary>
    class ConnectionStringProvider
    {
        const string CONNECTIONSTRING = "CONNECTIONSTRING";

        #region ConnectionString
        /// <summary>
        /// Retreive connection string from config file
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                return WebConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            }
        }

        #endregion
    }
}
