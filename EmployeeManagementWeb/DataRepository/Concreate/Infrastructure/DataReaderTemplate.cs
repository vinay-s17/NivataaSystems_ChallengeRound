﻿using System;
using System.Collections.Generic;
using System.Data;

/**********************************************************************************************************************************/
/* File Name: StoreProc.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace NS.Repositories.DataAccess
{
    /// <summary>
    /// DataReader Template class to start executing the SPs
    /// </summary>
    class DataReaderTemplate
    {
        /// <summary>
        /// Data Access DB Type enum
        /// </summary>
        public enum DataAccessType
        {
            Sql,
            Others
        }

        #region Properties

        private string _procName = string.Empty;
        private StoreProc storeProc;
        
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="storeProc"></param>
        public DataReaderTemplate(StoreProc storeProc)
        {
            this.storeProc = storeProc;
        }
        #endregion

        #region Execute
        /// <summary>
        /// Execute Stored Procedure
        /// </summary>
        /// <returns></returns>
        public void Execute()
        {
            IDbConnection connection = null;
            try
            {
                _procName = storeProc.StoreProcName;
                GenericDataAccess dataaccess = CreateDataAccess(DataAccessType.Sql);
                storeProc.Initialize(dataaccess);
                dataaccess.MethodName = _procName;

                using (connection = dataaccess.CreateConnection(storeProc.ConnectionString))
                {
                    //Open Connection
                    connection.Open();
                    using (IDbCommand command = dataaccess.GetStoredProcCommand(_procName))
                    {
                        command.Connection = connection;
                        dataaccess.Command = command;

                        //Map input parameters
                        if (!storeProc.MapInput())
                            return;

                        //Execute DB command and map output parameters
                        IDataReader reader = dataaccess.ExecuteReader();
                        storeProc.MapOutput(reader);
                        reader.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //ExceptionManagement.ExceptionManagement.HandleDALException(ex);
                throw ex;
            }
        }

        private GenericDataAccess CreateDataAccess(DataAccessType type)
        {
            if (type == DataAccessType.Sql)
                return new SqlDBAccess();
            return new EnterpriseLibraryDbAccess();
        }
        #endregion
    }
}
