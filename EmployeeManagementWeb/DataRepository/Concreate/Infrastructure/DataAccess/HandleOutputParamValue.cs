﻿using System;
/**********************************************************************************************************************************/
/* File Name: StoreProc.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace NS.Repositories.DataAccess
{
    /// <summary>
    /// Class to get Outlput Parameter values from Result set
    /// </summary>
    /// <typeparam name="T"></typeparam>
    static class HandleOutputParamValue<T>
    {
        /// <summary>
        /// Get item value from DataReader
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static T Get(System.Data.IDataReader reader, String Name)
        {
            if (!reader.IsDBNull(reader.GetOrdinal(Name)))
            {
                return (T)reader[Name];
            }
            else
            {
                return default(T);
            }
        }
    }
}
