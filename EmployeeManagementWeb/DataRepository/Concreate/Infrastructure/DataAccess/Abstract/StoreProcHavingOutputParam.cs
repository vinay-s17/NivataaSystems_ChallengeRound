﻿using System.Collections.Generic;

/**********************************************************************************************************************************/
/* File Name: StoreProc.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace NS.Repositories.DataAccess
{
    /// <summary>
    /// Class for Stored Proceduring with Output Parameter
    /// </summary>
    abstract class StoreProcHavingOutputParam : StoreProc
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString"></param>
        protected StoreProcHavingOutputParam(string connectionString)
            : base(connectionString)
        {

        }

        /// <summary>
        /// Abstract Dictionary to hold Out Parameters in a key value pair
        /// </summary>
        public abstract Dictionary<string, object> ParamNameValuePairList { get; set; }

        /// <summary>
        /// Abstact method to MapOutput Parameter
        /// </summary>
        public abstract void MapOutputParam();
    }
}
