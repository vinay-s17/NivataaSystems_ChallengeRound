﻿using System.Data;

/**********************************************************************************************************************************/
/* File Name: StoreProc.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace NS.Repositories.DataAccess
{
    /// <summary>
    /// Abstract class to define all stored procedure class
    /// </summary>
    abstract class StoreProc
    {
        #region Properties
        /// <summary>
        /// Generic Data Access reference to perform DB calls
        /// </summary>
        protected GenericDataAccess dataAccess { get; set; }

        //Connection string
        public string ConnectionString { get; set; }

        /// <summary>
        /// Reference to data reader for executing SP
        /// </summary>
        private DataReaderTemplate dataReader;

        /// <summary>
        /// Stored proc Name. To be set from derived class
        /// </summary>
        public string StoreProcName { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public StoreProc(string ConnectionString)
        {
            dataReader = new DataReaderTemplate(this);
            this.ConnectionString = ConnectionString;
        }
        #endregion

        #region AddInParameter
        /// <summary>
        /// Add input parameter
        /// </summary>
        /// <param name="strParamName"></param>
        /// <param name="dbType"></param>
        /// <param name="paramValue"></param>
        protected void AddInParameter(string strParamName, ParamType dbType, object paramValue, string paramTypeName = null)
        {
            if (dataAccess != null)
            {
                dataAccess.AddInParameter(strParamName, dbType, paramValue, paramTypeName);
            }
        } 
        #endregion
        
        #region AddOutParameter
        /// <summary>
        /// Add Out Parameter
        /// </summary>
        /// <param name="strParamName"></param>
        /// <param name="dbType"></param>
        /// <param name="size"></param>
        protected void AddOutParameter(string strParamName, ParamType dbType, int size)
        {
            if (dataAccess != null)
            {
                dataAccess.AddOutParameter(strParamName, dbType, size);
            }
        } 
        #endregion
        
        #region Initialize
        /// <summary>
        /// Initialize Stored Procedure with GenericDataAccess reference
        /// </summary>
        /// <param name="da"></param>
        public void Initialize(GenericDataAccess da)
        {
            dataAccess = da;
        } 
        #endregion

        #region OutParameter
        /// <summary>
        /// Get Out parameter after executing SP
        /// </summary>
        public IDbDataParameter OutParameter
        {
            get
            {
                if (dataAccess != null)
                {
                    return dataAccess.OutParameter;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion

        #region ExecuteSP
        /// <summary>
        /// Excute Stored Procedure
        /// </summary>
        /// <returns></returns>
        public void ExecuteSP()
        {
            dataReader.Execute();
        } 
        #endregion

        #region Abstact Methods
        /// <summary>
        /// Abstract method to map input
        /// </summary>
        /// <returns></returns>
        public abstract bool MapInput();

        /// <summary>
        /// Abstract method to Map Output
        /// </summary>
        /// <param name="reader"></param>
        public abstract void MapOutput(IDataReader reader); 
        #endregion
    }
}
