﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
/**********************************************************************************************************************************/
/* File Name: SqlDBAccess.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace NS.Repositories.DataAccess
{
    class SqlDBAccess : GenericDataAccess
    {

        #region GetParamType
        /// <summary>
        /// C# type to DbType mapping
        /// </summary>
        /// <param name="paramType"></param>
        /// <returns></returns>
        static SqlDbType GetParamType(ParamType paramType)
        {
            switch (paramType)
            {
                case ParamType.String:
                    return SqlDbType.VarChar;
                case ParamType.Byte:
                    return SqlDbType.TinyInt;
                case ParamType.Int16:
                    return SqlDbType.SmallInt;
                case ParamType.Int32:
                    return SqlDbType.Int;
                case ParamType.Int64:
                    return SqlDbType.BigInt;
                case ParamType.Double:
                    return SqlDbType.Float;
                case ParamType.Date:
                    return SqlDbType.Date;
                case ParamType.DateTime:
                    return SqlDbType.DateTime;
                case ParamType.Xml:
                    return SqlDbType.Xml;
                case ParamType.Binary:
                    return SqlDbType.Binary;
                case ParamType.Boolean:
                    return SqlDbType.Bit;
                case ParamType.Guid:
                    return SqlDbType.UniqueIdentifier;
                case ParamType.Structured:
                    return SqlDbType.Structured;
                case ParamType.Decimal:
                    return SqlDbType.Decimal;
                case ParamType.VarBinary:
                    return SqlDbType.VarBinary;
                default:
                    throw new ArgumentException("Invalid DataType");
            }
        }

        #endregion

        #region Create Connection

        /// <summary>
        /// Create SQL Server connection
        /// </summary>
        /// <param name="connectionString">ConnectionString</param>
        /// <returns></returns>
        public override IDbConnection CreateConnection(string connectionString)
        {
            try
            {
                Db = new SqlDatabase(connectionString);
                return Db.CreateConnection() as IDbConnection;
            }
            catch(Exception ex)
            {
                //throw ExceptionManagement.ExceptionManagement.HandleDALException(ex, "Error while creating database connection - Connection String : " + connectionString);
                return null;
            }
        }

        #endregion

        #region Add Parameter

        /// <summary>
        /// Add In Parameter
        /// </summary>
        /// <param name="strParamName">Name of the parameter</param>
        /// <param name="type">Parameter Type</param>
        /// <param name="paramValue">Value</param>
        public override void AddInParameter(string strParamName, ParamType type, object paramValue, string paramTypeName = null)
        {
            if (Command != null)
            {
                SqlParameter parameter = new SqlParameter();
                parameter.Direction = ParameterDirection.Input;
                parameter.SqlDbType = GetParamType(type);
                parameter.ParameterName = strParamName;
                parameter.Value = paramValue;
                if (!string.IsNullOrEmpty(paramTypeName))
                    parameter.TypeName = paramTypeName;
                Command.Parameters.Add(parameter);
            }
        }

        
        /// <summary>
        /// Add Out Parameter
        /// </summary>
        /// <param name="strParamName">Parameter Name</param>
        /// <param name="type">Type</param>
        /// <param name="size">Size</param>
        public override void AddOutParameter(string strParamName, ParamType type, int size)
        {
            if (Command != null)
            {
                SqlParameter parameter = new SqlParameter();
                parameter.Direction = ParameterDirection.Output;
                parameter.SqlDbType = GetParamType(type);
                parameter.ParameterName = strParamName;
                parameter.Size = size;
                Command.Parameters.Add(parameter);
                this.OutParameter = parameter;
            }
        }
        
        
        #endregion

        #region Command Creation
        /// <summary>
        /// Get Command
        /// </summary>
        /// <param name="Storedprocname"></param>
        /// <returns></returns>
        public override IDbCommand GetStoredProcCommand(string Storedprocname)
        {
            return Db.GetStoredProcCommand(Storedprocname) as IDbCommand;
        }

        #endregion

        #region Execute DB Objects

        /// <summary>
        /// Execute Data Reader
        /// </summary>
        /// <returns></returns>
        public override IDataReader ExecuteReader()
        {
            IDataReader dataReader = null;

            if (Command != null)
                dataReader = Command.ExecuteReader();

            return dataReader;
        }
        
        /// <summary>
        /// Execute NonQuery
        /// </summary>
        /// <returns></returns>
        public override int ExecuteNonQuery()
        {
            int rowsAffected = 0;
            if (Command != null)
                rowsAffected = Command.ExecuteNonQuery();

            return rowsAffected;
        }

        #endregion
    }
}
