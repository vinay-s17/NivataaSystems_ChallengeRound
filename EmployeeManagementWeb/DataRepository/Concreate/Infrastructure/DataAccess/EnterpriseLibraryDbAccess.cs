﻿using System;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

/**********************************************************************************************************************************/
/* File Name: StoreProc.cs
 * Created By: Vinay
 * Created On: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/
namespace NS.Repositories.DataAccess
{
    /// <summary>
    /// Data Access using Enterprise Library Data Access Application Block
    /// </summary>
    class EnterpriseLibraryDbAccess : GenericDataAccess
    {
        #region GetParamType
        /// <summary>
        /// C# type to DbType mapping
        /// </summary>
        /// <param name="paramType"></param>
        /// <returns></returns>
        static DbType GetParamType(ParamType paramType)
        {
            switch (paramType)
            {
                case ParamType.String:
                    return DbType.String;
                case ParamType.Int16:
                    return DbType.Int16;
                case ParamType.Int32:
                    return DbType.Int32;
                case ParamType.Int64:
                    return DbType.Int64;
                case ParamType.Double:
                    return DbType.Double;
                case ParamType.Date:
                    return DbType.Date;
                case ParamType.DateTime:
                    return DbType.DateTime;
                case ParamType.Xml:
                    return DbType.Xml;
                case ParamType.Binary:
                    return DbType.Binary;
                case ParamType.Boolean:
                    return DbType.Boolean;
                case ParamType.Guid:
                    return DbType.Guid;
                case ParamType.Byte:
                    return DbType.Byte;
                case ParamType.Decimal:
                    return DbType.Decimal;
                default:
                    throw new ArgumentException("Invalid DataType");
            }
        }
        #endregion

        #region CreateConnection
        /// <summary>
        /// Create DB Connection with connection string
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public override IDbConnection CreateConnection(string connectionString)
        {
            try
            {
                Db = new SqlDatabase(connectionString);
                return (IDbConnection)Db.CreateConnection();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region GetStoredProcCommand
        /// <summary>
        /// Get DBCommand with SP name
        /// </summary>
        /// <param name="Storedprocname">SP Name</param>
        /// <returns></returns>
        public override IDbCommand GetStoredProcCommand(string Storedprocname)
        {
            return (IDbCommand)Db.GetStoredProcCommand(Storedprocname);
        }
        #endregion

        #region AddInParameter
        /// <summary>
        /// In Parameters
        /// </summary>
        /// <param name="strParamName">Parameter name</param>
        /// <param name="dbType">DB Type</param>
        /// <param name="paramValue">Value</param>
        public override void AddInParameter(string strParamName, ParamType dbType, object paramValue, string paramTypeName=null)
        {
            if (Command != null)
            {
                IDbDataParameter parameter = Command.CreateParameter();
                parameter.Direction = ParameterDirection.Input;
                parameter.DbType = EnterpriseLibraryDbAccess.GetParamType(dbType);
                parameter.ParameterName = strParamName;
                parameter.Value = paramValue;
                Command.Parameters.Add(parameter);
            }
        }
        #endregion

        #region AddOutParameter
        /// <summary>
        /// Add out parameter
        /// </summary>
        /// <param name="strParamName"></param>
        /// <param name="dbType"></param>
        /// <param name="size"></param>
        public override void AddOutParameter(string strParamName, ParamType dbType, int size)
        {
            if (Command != null)
            {
                IDbDataParameter parameter = Command.CreateParameter();
                parameter.Direction = ParameterDirection.Output;
                parameter.DbType = EnterpriseLibraryDbAccess.GetParamType(dbType);
                parameter.ParameterName = strParamName;
                parameter.Size = size;
                Command.Parameters.Add(parameter);
                this.OutParameter = parameter;
            }
        }
        #endregion

        #region ExecuteReader
        /// <summary>
        /// Execute through DataReader
        /// </summary>
        /// <returns></returns>
        public override IDataReader ExecuteReader()
        {
            IDataReader dataReader = null;

            if (Command != null)
                dataReader = Command.ExecuteReader();

            return dataReader;
        }
        #endregion
        
        #region ExecuteNonQuery
        /// <summary>
        /// Write Operations
        /// </summary>
        /// <returns></returns>
        public override int ExecuteNonQuery()
        {
            int rowsAffected = 0;
            if (Command != null)
                rowsAffected = Command.ExecuteNonQuery();

            return rowsAffected;
        }
        #endregion
    }
}