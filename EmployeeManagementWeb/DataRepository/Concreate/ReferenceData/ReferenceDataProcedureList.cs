﻿using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;
/**********************************************************************************************************************************/
/* File Name: ReferenceDataProcedureList.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    class ReferenceDataProcedureList
    {
        #region Get All Cities
        /// <summary>
        /// Get All Cities as List
        /// </summary>
        /// <returns></returns>
        public static List<City> GetAllCities(Guid stateId)
        {
            try
            {
                return GetAllCitiesSP.Execute(stateId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get All Skills
        /// <summary>
        /// Get All Skills as List
        /// </summary>
        /// <returns></returns>
        public static List<State> GetAllStates()
        {
            try
            {
                return GetAllStatesSP.Execute();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
       

    }
}
