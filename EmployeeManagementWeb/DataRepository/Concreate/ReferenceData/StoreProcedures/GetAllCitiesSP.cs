﻿using System;
using NS.EntityRepositories.Database;
using System.Collections.Generic;
using EmployeeManagement.Entity;
using NS.Repositories.DataAccess;

/**********************************************************************************************************************************/
/* File Name: GetAllCitiesSP.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: Used to get All Cities list based on State ID
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    class GetAllCitiesSP : StoreProc
    {
        #region Parameters and Constant

        public List<City> lstCities = null;
        public Guid stateId { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// GetAllCitiesSP Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public GetAllCitiesSP(Guid stateId, string ConnectionString) : base(ConnectionString)
        {
            this.StoreProcName = "NS_GETALL_CITIES_PROC";
            this.stateId = stateId;
        }
        #endregion

        #region Map Input
        /// <summary>
        /// Map Input
        /// </summary>
        /// <returns></returns>
        public override bool MapInput()
        {
            AddInParameter("STATE_ID", ParamType.Guid, stateId);
            return true;
        }
        #endregion

        #region Map Output

        /// <summary>
        /// Map Output
        /// </summary>
        /// <param name="reader"></param>
        public override void MapOutput(System.Data.IDataReader reader)
        {
            if (reader != null)
            {
                lstCities = new List<City>();
                City cityDao = null;
                while (reader.Read())
                {
                    cityDao = new City();
                    cityDao.CityId = HandleOutputParamValue<Guid>.Get(reader, "CityId");
                    cityDao.CityName = HandleOutputParamValue<string>.Get(reader, "CityName");
                    lstCities.Add(cityDao);
                    cityDao = null;
                }
            }
        }

        #endregion

        #region ExcecuteSP
        /// <summary>
        /// Method for Excecution of SP
        /// </summary>
        /// <returns></returns>
        public static List<City> Execute(Guid stateId)
        {
            GetAllCitiesSP storedProcedure = new GetAllCitiesSP(stateId, ConnectionStringProvider.ConnectionString);
            storedProcedure.ExecuteSP();
            return storedProcedure.lstCities;
        }
        #endregion


    }
}
