﻿using System;
using NS.EntityRepositories.Database;
using NS.Repositories.DataAccess;
using System.Collections.Generic;
using EmployeeManagement.Entity;

/**********************************************************************************************************************************/
/* File Name: GetAllCitiesSP.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: Database JobSeeker Respository class to handle all the database related operations with respect to JobSeeker Module
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    class GetAllStatesSP : StoreProc
    {
        #region Parameters and Constant

        public List<State> lstStates = null;

        #endregion

        #region Constructors
        /// <summary>
        /// GetAllCitiesSP Constructor
        /// </summary>
        /// <param name="ConnectionString"></param>
        public GetAllStatesSP(string ConnectionString) : base(ConnectionString)
        {
            this.StoreProcName = "NS_GETALL_STATES_PROC";
        }
        #endregion

        #region Map Input
        /// <summary>
        /// Map Input
        /// </summary>
        /// <returns></returns>
        public override bool MapInput()
        {
           return true;
        }
        #endregion

        #region Map Output

        /// <summary>
        /// Map Output
        /// </summary>
        /// <param name="reader"></param>
        public override void MapOutput(System.Data.IDataReader reader)
        {
            if (reader != null)
            {
                lstStates = new List<State>();
                State stateDao = null;
                while (reader.Read())
                {
                    stateDao = new State();
                    stateDao.StateId = HandleOutputParamValue<Guid>.Get(reader, "StateId");
                    stateDao.StateName = HandleOutputParamValue<string>.Get(reader, "StateName");
                    lstStates.Add(stateDao);
                    stateDao = null;
                }
            }
        }

        #endregion

        #region ExcecuteSP
        /// <summary>
        /// Method for Excecution of SP
        /// </summary>
        /// <returns></returns>
        public static List<State> Execute()
        {
            GetAllStatesSP storedProcedure = new GetAllStatesSP(ConnectionStringProvider.ConnectionString);
            storedProcedure.ExecuteSP();
            return storedProcedure.lstStates;
        }
        #endregion

    }
}
