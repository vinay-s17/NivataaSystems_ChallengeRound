﻿using EmployeeManagement.Entity;
using System;
using System.Collections.Generic;
/**********************************************************************************************************************************/
/* File Name: DatabaseReferenceDataRepository.cs
 * Author: Vinay
 * Created on: 09-Oct-2016
 * Modification History
 * Description: used for getting all States
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace DataRepository
{
    public class DatabaseReferenceDataRepository : IReferenceDataRepository
    {

        #region Get All Cities

        /// <summary>
        /// Get All Cities
        /// </summary>
        /// <returns></returns>

        public List<City> GetAllCities(Guid stateId)
        {
            return ReferenceDataProcedureList.GetAllCities(stateId);
        }
        #endregion

        #region Get All States as List

        /// <summary>
        /// Get All States
        /// </summary>
        /// <returns></returns>

        public List<State> GetAllStates()
        {
            return ReferenceDataProcedureList.GetAllStates();
        }
        #endregion
        
    }
}
