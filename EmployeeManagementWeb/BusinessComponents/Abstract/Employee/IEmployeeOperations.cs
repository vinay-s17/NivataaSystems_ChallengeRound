﻿using System;
using EmployeeManagement.Entity;
using System.Collections.Generic;

/**********************************************************************************************************************************/
/* File Name: IEmployeeOperations.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.BusinessComponents
{
    /// <summary>
    /// Interface for Employee Operations
    /// </summary>
    public interface IEmployeeOperations
    {
        #region Get All Employees as List

        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <returns></returns>
        List<Employee> GetAllEmployees(string filterText);

        #endregion

        #region Get Employe by UId

        /// <summary>
        /// Get Employe by Id
        /// </summary>
        /// <returns></returns>
        Employee GetEmployeeById(Guid employeeUid);

        #endregion

        #region Create or Update Employee

        /// <summary>
        /// Create or Update Employee
        /// </summary>
        /// <returns></returns>
        void CreateOrUpdateEmployee(Employee employeeDao);

        #endregion


        #region Delete employee

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        void DeleteEmployee(Guid employeeUid);

        #endregion

    }
}
