﻿using System;
using EmployeeManagement.Entity;
using System.Collections.Generic;

/**********************************************************************************************************************************/
/* File Name: IReferenceDataOperations.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.BusinessComponents
{
    /// <summary>
    /// Interface for ReferenceData Operations
    /// </summary>
    public interface IReferenceDataOperations
    {
        #region Get All States as List

        /// <summary>
        /// Get All Cities
        /// </summary>
        /// <returns></returns>
        List<State> GetAllStates();

        #endregion

        #region Get All Cities as List

        /// <summary>
        /// Get All Cities
        /// </summary>
        /// <returns></returns>
        List<City> GetAllCities(Guid stateId);

        #endregion
        
    }
}
