﻿using System;
using System.Collections.Generic;
using EmployeeManagement.Entity;
using DataRepository;


/**********************************************************************************************************************************/
/* File Name: EmployeeOperations.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.BusinessComponents
{
    /// <summary>
    /// Class to handle the Employee Data related Operations
    /// </summary>
    public class EmployeeOperations : IEmployeeOperations
    {
        #region Properties
        /// <summary>
        /// Properties of the class
        /// </summary>

        private IEmployeeRepository refEmployeeRepository;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor of the Class
        /// </summary>
        /// <param name="refEmployeeRepository"></param>

        public EmployeeOperations(IEmployeeRepository refEmployeeRepository)
        {
            this.refEmployeeRepository = refEmployeeRepository;
        }

        #endregion

        #region Get All Employees as List

        /// <summary>
        /// Get All Employees
        /// </summary>
        /// <returns></returns>
        public List<Employee> GetAllEmployees(string filterText)
        {
            return refEmployeeRepository.GetAllEmployees(filterText);
        }

        #endregion
      
        #region Get Employe by UId

        /// <summary>
        /// Get Employe by Id
        /// </summary>
        /// <returns></returns>
        public Employee GetEmployeeById(Guid employeeUid)
        {
            return refEmployeeRepository.GetEmployeeById(employeeUid);
        }

        #endregion


        #region Create or Update Employee

        /// <summary>
        /// Create or Update Employee
        /// </summary>
        /// <returns></returns>
        public void CreateOrUpdateEmployee(Employee employeeDao)
        {
            refEmployeeRepository.CreateOrUpdateEmployee(employeeDao);
        }

        #endregion

        #region Delete employee

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        public void DeleteEmployee(Guid employeeUid)
        {
           refEmployeeRepository.DeleteEmployee(employeeUid);
        }

        #endregion
    }
}
