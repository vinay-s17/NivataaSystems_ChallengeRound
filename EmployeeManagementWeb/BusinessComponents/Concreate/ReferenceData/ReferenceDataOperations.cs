﻿using System;
using System.Collections.Generic;
using EmployeeManagement.Entity;
using DataRepository;


/**********************************************************************************************************************************/
/* File Name: ReferenceDataOperations.cs
 * Author: Vinay
 * Created on: 19-Nov-2016
 * Modification History
 * Modified By      Modified Date       Remarks
 * 
/**********************************************************************************************************************************/

namespace EmployeeManagement.BusinessComponents
{
    /// <summary>
    /// Class to handle the Reference Data related Operations
    /// </summary>
    public class ReferenceDataOperations : IReferenceDataOperations
    {
        #region Properties
        /// <summary>
        /// Properties of the class
        /// </summary>

        private IReferenceDataRepository refDataRepository;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor of the Class
        /// </summary>
        /// <param name="refDataRepository"></param>

        public ReferenceDataOperations(IReferenceDataRepository refDataRepository)
        {
            this.refDataRepository = refDataRepository;
        }

        #endregion

        #region Get All States as List

        /// <summary>
        /// Get All States
        /// </summary>
        /// <returns></returns>

        public List<State> GetAllStates()
        {
            return refDataRepository.GetAllStates();
        }
        #endregion

        #region Get All Cities

        /// <summary>
        /// Get All Cities as List
        /// </summary>
        /// <returns></returns>

        public List<City> GetAllCities(Guid stateId)
        {
            return refDataRepository.GetAllCities(stateId);
        }
        #endregion
    }
}
