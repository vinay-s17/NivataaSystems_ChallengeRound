--DROP	TABLE [EMPLOYEE_ADDRESS]
--DROP	TABLE [MD_ADDR_TYPE]
--DROP	TABLE	[EMPLOYEE]
--DROP	TABLE	[MD_CITY]
--DROP	TABLE [MD_STATE]

--DROP	VIEW [VUE_EMPLOYEE_ADDRESS]
--DROP	VIEW [VUE_MD_ADDR_TYPE]
--DROP	VIEW [VUE_EMPLOYEE]
--DROP	VIEW [VUE_MD_CITY]
--DROP	VIEW [VUE_MD_STATE]		
		------------------------------------------------------------
		/****** Create Object:  Table [dbo].[MD_STATE]    ******/
		------------------------------------------------------------

		CREATE TABLE [dbo].[MD_STATE]
		(	
			[STATE_ID]						[UNIQUEIDENTIFIER]			NOT NULL, 
			[STATE_NAME]					[VARCHAR](50)				NOT NULL, 
			[CRE_DATE]						[DATETIME2](0)				DEFAULT GETDATE(),
			[MODIFIED_DATE]					[DATETIME2](0)				NULL,
			-- UNIQUE Identifier of the table

			CONSTRAINT [PK_MD_STATE] PRIMARY KEY NONCLUSTERED 
			(
				[STATE_ID] ASC
			)

			WITH 
			(
				PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		) ON [PRIMARY]

		GO

		SET ANSI_PADDING OFF
		GO

		ALTER TABLE [dbo].[MD_STATE] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [STATE_ID]
		GO

		------------------------------------------------------------
		/****** Create Object:  Table [dbo].[MD_CITY]    ******/
		------------------------------------------------------------

		CREATE TABLE [dbo].[MD_CITY]
		(	
			[CITY_ID]						[UNIQUEIDENTIFIER]			NOT NULL, 
			[CITY_NAME]						[VARCHAR](50)				NOT NULL,
			[CRE_DATE]						[DATETIME2](0)				DEFAULT GETDATE(),
			[STATE_ID]						[UNIQUEIDENTIFIER]			CONSTRAINT [FK_CITY_STATE_ID] REFERENCES [MD_STATE]([STATE_ID]),
			[MODIFIED_DATE]					[DATETIME2](0)				DEFAULT GETDATE(),

			-- UNIQUE Identifier of the table

			 CONSTRAINT [PK_MD_CITY] PRIMARY KEY NONCLUSTERED 
			(
				[CITY_ID] ASC
			)

			WITH 
			(
				PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		) ON [PRIMARY]

		GO

		SET ANSI_PADDING OFF
		GO

		ALTER TABLE [dbo].[MD_CITY] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [CITY_ID]
		GO


		------------------------------------------------------------
		/****** Create Object:  Table [dbo].[EMPLOYEE]    ******/
		------------------------------------------------------------
		CREATE TABLE [dbo].[EMPLOYEE]
		(	
			[EMP_UID]					[UNIQUEIDENTIFIER]			NOT NULL,  
			[EMP_DISPLAY_ID]			INT IDENTITY (1000, 1 )		NOT NULL UNIQUE,
			[EMP_FNAME]					[VARCHAR](30)				NOT NULL,  
			[EMP_LNAME]					[VARCHAR](30)				NOT NULL, 
			[CRE_DATE]					[DATETIME2](0)				DEFAULT GETDATE(),
			[MODIFIED_DATE]				[DATETIME2](0)				NULL,
			-- UNIQUE Identifier of the table

			 CONSTRAINT [PK_EMPLOYEE] PRIMARY KEY NONCLUSTERED 
			(
				[EMP_UID] ASC
			)

			WITH 
			(
				PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		)ON [PRIMARY]

		GO

		SET ANSI_PADDING OFF
		GO

		ALTER TABLE [dbo].[EMPLOYEE] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [EMP_UID]
		GO


		------------------------------------------------------------
		/****** Create Object:  Table [dbo].[ADDR_TYPE]    ******/
		------------------------------------------------------------

		CREATE TABLE [dbo].[MD_ADDR_TYPE]
		(	
			[ADDR_TYPE_ID]					INT IDENTITY(1,1)			NOT NULL UNIQUE, 
			[ADDR_TYPE_NAME]				[VARCHAR](50)				NOT NULL, 
			[CRE_DATE]						[DATETIME2](0)				DEFAULT GETDATE(),
			[MODIFIED_DATE]					[DATETIME2](0)				NULL,
			-- UNIQUE Identifier of the table

			CONSTRAINT [PK_MD_ADDR_TYPE] PRIMARY KEY NONCLUSTERED 
			(
				[ADDR_TYPE_ID] ASC
			)

			WITH 
			(
				PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		) ON [PRIMARY]

		GO

		SET ANSI_PADDING OFF
		GO


		------------------------------------------------------------
		/****** Create Object:  Table [dbo].[USER_SEEKER]    ******/
		------------------------------------------------------------

		CREATE TABLE [dbo].[EMPLOYEE_ADDRESS]
		(	
			[EMP_ADDR_ID]				[UNIQUEIDENTIFIER]			NOT NULL,  
			[EMP_ADDR_BUILDING_NAME]	[VARCHAR](300)				NOT NULL,  
			[EMP_ADDR_POSTAL_CODE]		[VARCHAR](6)				NOT NULL, 
			[EMP_UID]					[UNIQUEIDENTIFIER]			CONSTRAINT [FK_EMPLOYEE_ADDRESS] REFERENCES [EMPLOYEE]([EMP_UID]),
			[CITY_ID]					[UNIQUEIDENTIFIER]			CONSTRAINT [FK_EMPLOYEE_ADDRESS_CITY] REFERENCES [MD_CITY]([CITY_ID]),
			[STATE_ID]					[UNIQUEIDENTIFIER]			CONSTRAINT [FK_EMPLOYEE_ADDRESS_STATE] REFERENCES [MD_STATE]([STATE_ID]),
			[ADDR_TYPE_ID]				[INT]						CONSTRAINT [FK_ADDR_TYPE_ID] REFERENCES [MD_ADDR_TYPE]([ADDR_TYPE_ID]),
			[CRE_DATE]					[DATETIME2](0)				DEFAULT GETDATE(),
			[MODIFIED_DATE]				[DATETIME2](0)				NULL,
			-- UNIQUE Identifier of the table

			 CONSTRAINT [PK_EMPLOYEE_ADDRESS] PRIMARY KEY NONCLUSTERED 
			(
				[EMP_ADDR_ID] ASC
			)

			WITH 
			(
				PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
			) ON [PRIMARY]
		)ON [PRIMARY]

		GO

		SET ANSI_PADDING OFF
		GO

		ALTER TABLE [dbo].[EMPLOYEE_ADDRESS] ADD  DEFAULT (NEWSEQUENTIALID()) FOR [EMP_ADDR_ID]
		GO


		----------------------------------------------------------------------------------------------------------------------------------------

		------------------------------------------------------------
		/****** Create Object:  VIEW [dbo].[VUE_MD_STATE]   ******/
		------------------------------------------------------------
		CREATE VIEW	[dbo].[VUE_MD_STATE] 
		(	
			[STATE_ID],
			[STATE_NAME], 
			[CRE_DATE],
			[MODIFIED_DATE]				
		) As

		SELECT	[STATE_ID],
				[STATE_NAME], 
				[CRE_DATE],
				[MODIFIED_DATE]			

		FROM	[MD_STATE]
		
		GO

		------------------------------------------------------------
		/****** Create Object:  VIEW [dbo].[VUE_MD_CITY]   ******/
		------------------------------------------------------------
		CREATE VIEW	[dbo].[VUE_MD_CITY] 
		(	
			[CITY_ID],
			[CITY_NAME],  
			[STATE_ID],
			[CRE_DATE],
			[MODIFIED_DATE]				
		) As

		SELECT	[CITY_ID],
				[CITY_NAME],  
				[STATE_ID],
				[CRE_DATE],
				[MODIFIED_DATE]		

		FROM	[MD_CITY]
		
		GO

		------------------------------------------------------------
		/****** Create Object:  VIEW [dbo].[VUE_MD_ADDR_TYPE]   ******/
		------------------------------------------------------------
		CREATE VIEW	[dbo].[VUE_MD_ADDR_TYPE] 
		(	
			[ADDR_TYPE_ID], 
			[ADDR_TYPE_NAME], 
			[CRE_DATE],
			[MODIFIED_DATE]

		) As

		SELECT	
				[ADDR_TYPE_ID], 
				[ADDR_TYPE_NAME], 
				[CRE_DATE],
				[MODIFIED_DATE]	

		FROM	[MD_ADDR_TYPE]
		
		GO

		------------------------------------------------------------
		/****** Create Object:  VIEW [dbo].[VUE_EMPLOYEE_ADDRESS]   ******/
		------------------------------------------------------------
		CREATE VIEW	[dbo].[VUE_EMPLOYEE_ADDRESS] 
		(	
			[EMP_ADDR_ID],  
			[EMP_ADDR_BUILDING_NAME],  
			[EMP_ADDR_POSTAL_CODE], 
			[EMP_UID],
			[CITY_ID],
			[STATE_ID],
			[ADDR_TYPE_ID],
			[CRE_DATE],
			[MODIFIED_DATE]

		) As

		SELECT	[EMP_ADDR_ID],  
				[EMP_ADDR_BUILDING_NAME],  
				[EMP_ADDR_POSTAL_CODE], 
				[EMP_UID],
				[CITY_ID],
				[STATE_ID],
				[ADDR_TYPE_ID],
				[CRE_DATE],
				[MODIFIED_DATE]

		FROM	[EMPLOYEE_ADDRESS]
		
		GO

		------------------------------------------------------------
		/****** Create Object:  VIEW [dbo].[VUE_EMPLOYEE]   ******/
		------------------------------------------------------------
		CREATE VIEW	[dbo].[VUE_EMPLOYEE] 
		(	
			[EMP_UID],  
			[EMP_DISPLAY_ID],
			[EMP_FNAME],  
			[EMP_LNAME], 
			[CRE_DATE],
			[MODIFIED_DATE]

		) As

		SELECT	[EMP_UID],  
				[EMP_DISPLAY_ID],
				[EMP_FNAME],  
				[EMP_LNAME], 
				[CRE_DATE],
				[MODIFIED_DATE]

		FROM	[EMPLOYEE]
		
		GO
