SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (	SELECT	1
			FROM	INFORMATION_SCHEMA.ROUTINES
			WHERE ROUTINE_TYPE = 'PROCEDURE'
			AND ROUTINE_NAME = 'NS_GET_EMPLOYE_BY_ID_PROC')

  DROP PROCEDURE [dbo].[NS_GET_EMPLOYE_BY_ID_PROC]
GO
-- =============================================================      
-- Author:  VINAY
-- Modified date: 19-Nov-2016 
-- Description: This procedure is used to get Employee details by UID 
-- =============================================================      
-- ============================================================= 

CREATE PROCEDURE [dbo].[NS_GET_EMPLOYE_BY_ID_PROC]
(
	@EMP_UID UNIQUEIDENTIFIER
)
AS
BEGIN
		SELECT		[EMP_UID] AS [EmpUid],
					[EMP_DISPLAY_ID] AS [EmpDisplayid],
					[EMP_FNAME] AS [EmpFName],
					[EMP_LNAME] AS [EmpLName],
					STUFF(
					(		
					SELECT '^' + ( convert(nvarchar(50), [VUE_EMPLOYEE_ADDRESS].[STATE_ID]) + '|' + convert(nvarchar(50), [VUE_EMPLOYEE_ADDRESS].[CITY_ID])+ '|' + [EMP_ADDR_BUILDING_NAME] + '|' + [EMP_ADDR_POSTAL_CODE])
					FROM		[VUE_EMPLOYEE] [V2]
					INNER JOIN
					[VUE_EMPLOYEE_ADDRESS]
					ON [V2].[EMP_UID] = [VUE_EMPLOYEE_ADDRESS].[EMP_UID]
					WHERE	[V1].[EMP_UID] = [V2].[EMP_UID]
					FOR XML PATH ('')), 1, 1, ''
					) AS [ADDRESS],
					[CRE_DATE]
		FROM		[VUE_EMPLOYEE] V1
		
		WHERE		[V1].[EMP_UID] = @EMP_UID
END
