SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (	SELECT	1
			FROM	INFORMATION_SCHEMA.ROUTINES
			WHERE ROUTINE_TYPE = 'PROCEDURE'
			AND ROUTINE_NAME = 'NS_GETALL_STATES_PROC')

  DROP PROCEDURE [dbo].[NS_GETALL_STATES_PROC]
GO
-- =============================================================      
-- Author:  VINAY
-- Modified date: 19-Nov-2016 
-- Description: This procedure is used to get all States list 
-- =============================================================      
-- ============================================================= 

CREATE PROCEDURE [dbo].[NS_GETALL_STATES_PROC] 
AS
BEGIN	
		SELECT		[STATE_ID] AS [StateId], 
					[STATE_NAME] AS [StateName] 
		FROM		[VUE_MD_STATE]
		ORDER BY	[STATE_NAME]
END