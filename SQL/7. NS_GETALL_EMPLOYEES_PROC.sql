SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (	SELECT	1
			FROM	INFORMATION_SCHEMA.ROUTINES
			WHERE ROUTINE_TYPE = 'PROCEDURE'
			AND ROUTINE_NAME = 'NS_GETALL_EMPLOYEES_PROC')

  DROP PROCEDURE [dbo].[NS_GETALL_EMPLOYEES_PROC]
GO
-- =============================================================      
-- Author:  VINAY
-- Modified date: 19-Nov-2016 
-- Description: This procedure is used to get all Employees. 
-- =============================================================      
-- ============================================================= 

CREATE PROCEDURE [dbo].[NS_GETALL_EMPLOYEES_PROC]
(
	@FILTER_TEXT VARCHAR(MAX) = NULL
)
AS
BEGIN	
		DECLARE @T TABLE (ID UNIQUEIDENTIFIER)
		
		SELECT		@FILTER_TEXT = ISNULL(@FILTER_TEXT, '')
		INSERT INTO @T
		SELECT	[VE].[EMP_UID]	FROM	[VUE_EMPLOYEE] [VE]
		JOIN	[VUE_EMPLOYEE_ADDRESS] [VEA]
		ON		[VE].[EMP_UID] = [VEA].[EMP_UID]
		JOIN	[VUE_MD_STATE]
		ON		[VEA].[STATE_ID] = [VUE_MD_STATE].[STATE_ID]
		JOIN	[VUE_MD_CITY]	
		ON			[VEA].[CITY_ID]  = [VUE_MD_CITY].[CITY_ID]
		WHERE		[VE].[EMP_DISPLAY_ID] LIKE ('%'+@FILTER_TEXT+'%') OR
					[VE].[EMP_FNAME] LIKE ('%'+@FILTER_TEXT+'%') OR
					[VE].[EMP_LNAME] LIKE ('%'+@FILTER_TEXT+'%') OR
					[STATE_NAME] LIKE ('%'+@FILTER_TEXT+'%') OR
					[CITY_NAME] LIKE ('%'+@FILTER_TEXT+'%') 
		
		
		SELECT		DISTINCT [EMP_UID] AS [EmpUid],
					[EMP_DISPLAY_ID] AS [EmpDisplayid],
					[EMP_FNAME] AS [EmpFName],
					[EMP_LNAME] AS [EmpLName],
					STUFF(
					(		
					SELECT '^' + ( convert(nvarchar(50), [STATE_NAME]) + '|' + convert(nvarchar(50), [CITY_NAME])+ '|' + [EMP_ADDR_BUILDING_NAME] + '|' + [EMP_ADDR_POSTAL_CODE])
					FROM		[VUE_EMPLOYEE] [V2]
					INNER JOIN
					[VUE_EMPLOYEE_ADDRESS]
					ON [V2].[EMP_UID] = [VUE_EMPLOYEE_ADDRESS].[EMP_UID]
					INNER JOIN VUE_MD_STATE ON [VUE_EMPLOYEE_ADDRESS].STATE_ID = VUE_MD_STATE.STATE_ID
					INNER JOIN VUE_MD_CITY ON [VUE_EMPLOYEE_ADDRESS].CITY_ID = VUE_MD_CITY.CITY_ID
					WHERE	[V1].[EMP_UID] = [V2].[EMP_UID]
					FOR XML PATH ('')), 1, 1, ''
					) AS [ADDRESS],
					[CRE_DATE]
		FROM		[VUE_EMPLOYEE] V1
		INNER JOIN	@T ON ID = [V1].[EMP_UID]
		ORDER BY	[CRE_DATE] DESC

END
