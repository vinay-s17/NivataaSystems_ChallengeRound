Nivaata Systems Challenge round application for Creating Employee Directory module (CRUD) has been developed and deployed in the below URL.
Demo URL: http://nivaatachallengeround.gear.host/
Source code URL: https://gitlab.com/vinay-s17/NivataaSystems_ChallengeRound.git
Solution file download as zip: 
Architecture pattern: N-Tier Architecture:
1 – Application layer
2 – Business Layer
3 – Data access Layer

Database:
Server details: mssql3.gear.host
User name: nivaataempdb
Password: Nivaata@123
Database Design:
EMPLOYEE	Value	Remarks
[EMP_UID]	UNIQUEIDENTIFIER	[PK]Unique Identifier of the Table(Salt)
[EMP_DISPLAY_ID]	INT	[UK]Unique Integer of the Table
[EMP_FNAME]	VARCHAR(30)	First name of Employee
[EMP_LNAME]	VARCHAR(30)	First name of Employee


EMPLOYEE_ADDRESS	Value	Remarks
[EMP_ADDR_ID]	UNIQUEIDENTIFIER	[PK]Unique Identifier of the Table(Salt)
[EMP_UID]	UNIQUEIDENTIFIER	[FK]Employee Identifier using Employee table
[EMP_ADDR_BUILDING_NAME]	VARCHAR(300)	Address of an Employee
[CITY_ID]	UNIQUEIDENTIFIER	[FK]City Identifier using City table
[STATE_ID]	UNIQUEIDENTIFIER	[FK]State Identifier using State table
[EMP_ADDR_POSTAL_CODE]	VARCHAR(6)	Postal code addres

MD_STATE	Value	Remarks
[STATE_ID]	UNIQUEIDENTIFIER	[PK]Unique Identifier of the Table
[STATE_NAME]	VARCHAR(30)	State name

MD_CITY	Value	Remarks
[STATE_ID]	UNIQUEIDENTIFIER	[FK]State Identifier of the Table from State table
[CITY_ID]	UNIQUEIDENTIFIER	[PK]Unique Identifier of the Table
[CITY_NAME]	VARCHAR(30)	City name

MD_ADDR_TYPE	Value	Remarks
[ADDR_TYPE_ID]	INT	[PK]ADDR_TYPE Identifier of the Table or identifying Home or Postal address
[ADDR_TYPE_NAME]	VARCHAR(50)	Address Type (Enum) name  

Frameworks Used:
1.	Syncfusion UI (For rich UI– DropdownList, Grid) 

I was chosen this framework, because 
•	Lots of inbuilt functionalities on Grid like – Sorting, Grouping, Exporting grid data to Excel, Pdf, Word, Printing, etc.
•	Lots of inbuilt functionalities on DropdownList like – Cascading, Grouping, MultiSelect items, etc.
•	All of its Licensee is Flat free as like open source. 

2.	Microsoft Enterprise Library (Data access block)
I was chosen this framework for data access mainly, because
•	Easy extensibility.
•	Easy integration with any data driven application.
•	Simplified development tasks to implement common data access functionality CRUD operations.

3.	Unity5 Dependency Injection
I was chosen this framework, because
•	Maintain Loosely coupled architecture.
•	Light weight and Reliable framework with high extensibility.

4.	ELMAH Error Logging modules and Handlers.
I was chosen this framework, because
•	Easily integrated into any application.
•	Open source and User friendly error log details.


Items supposed to complete, but unfortunately there has less time in order to complete.
1.	Micro service architecture pattern instead of using it in Monolithic architecture.
2.	Application Caching.
3.	Application Error logging by using Microsoft Enterprise library instead of using third party error logger (ELMAH)
Demo application Wireframe details:
1.	Dashboard
 
2.	Add / Edit 
 
3.	Delete 
 
